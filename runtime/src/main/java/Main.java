public class Main {
    public static void main(String[] args) {
        if(args.length < 1) {
            System.out.println("Usage: runtime [message]");
        }

        EchoProducer echoProducer = new EchoProducer();
        System.out.println(echoProducer.echo(args[0]));
    }
}
