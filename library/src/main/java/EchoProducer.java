import java.util.Objects;

public class EchoProducer {

    public String echo(String message) {
        Objects.requireNonNull(message);
        return message;
    }
}
